<?php
/*
 * This file is part of sineos-filemanager-bundle.
 *
 * (c) Oliver Lohoff, Contao4you.de
 *
 * @license LGPL-3.0-or-later
 */

namespace Sineos\FileManagerBundle\DataContainer;

use Contao\Backend;
use Contao\StringUtil;
use Contao\Image;
use Sineos\FileManagerBundle\LicenseHelper;

class FileListener extends Backend
{

    public function getUsageButton($arrData, $href, $label, $title, $icon, $attributes, $table, $ids, $childIds)
    {
        if($arrData["type"] == "file") {
            $href = $href . "&amp;id=" . $arrData["id"];
            return '<a href="' . $this->addToUrl($href) . '">' . Image::getHtml($icon, $label) . '</a>';
        }

        if(LicenseHelper::checkLicense() && $arrData["type"] == "folder") {
            $icon = 'bundles/filemanager/icons/unlink-outline.svg';
            $href = "key=nousage&amp;id=" . $arrData["id"];
            return '<a href="' . $this->addToUrl($href) . '">' . Image::getHtml($icon, $label) . '</a>';
        }
    }
}