<?php

$GLOBALS['TL_LANG']['tl_settings']['sineos_filemanager_license_legend'] = 'Sineos Filemanager';
$GLOBALS['TL_LANG']['tl_settings']['sineos_filemanager_license'][0] = 'Lizenzschlüssel';
$GLOBALS['TL_LANG']['tl_settings']['sineos_filemanager_license'][1] = 'Bitte geben Sie einen Lizenzschlüssel ein.';