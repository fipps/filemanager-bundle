<?php

use Sineos\FileManagerBundle\Modules\Usage;
use Sineos\FileManagerBundle\Modules\NoUsage;
use Contao\CoreBundle\ContaoCoreBundle;

if (version_compare(ContaoCoreBundle::getVersion(), '5.0', '>=')) {
    $GLOBALS['BE_MOD']['content']['files']['usage'] = array(Usage::class, 'showUsage');
    $GLOBALS['BE_MOD']['content']['files']['nousage'] = array(NoUsage::class, 'showNoUsage');    
} else {
    $GLOBALS['BE_MOD']['system']['files']['usage'] = array(Usage::class, 'showUsage');
    $GLOBALS['BE_MOD']['system']['files']['nousage'] = array(NoUsage::class, 'showNoUsage');
}